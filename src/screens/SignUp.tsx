/* eslint-disable no-shadow */
/* eslint-disable no-lone-blocks */
import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  KeyboardAvoidingView,
  Platform,
  Dimensions,
  ToastAndroid,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Button from '../components/Button';
import Input from '../components/Input';
import SignScaffold from '../components/SignScaffold';
import Tabs from './Tabs';
import {Formik, Field} from 'formik';
import * as yup from 'yup';
import inputValidation from '../helpers/Validation';
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const logo = require('../../assets/images/logo-colour.png');

interface SignUpProps {
  navigation: any;
}

const SignUp = ({navigation}: SignUpProps) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [invalidCredential, setInvalidCredential] = useState(false);
  const [emptyCredential, setEmptyCredential] = useState(false);
  const [emptyUsername, setEmptyUsername] = useState(false);
  const [emptyPassword, setEmptyPassword] = useState(false);
  const [invalidUsername, setInvalidUsername] = useState(false);
  const [invalidPassword, setInvalidPassword] = useState(false);
  const [emptyemail, setEmptyEmail] = useState(false);
  const [invalidEmail, setInvalidEmail] = useState(false);

  const behavior = Platform.OS === 'ios' ? 'padding' : undefined;

  const goToHome = () => {
    const inputValidate = new inputValidation();
    const isEmailValid = inputValidate.emailValidate(email);
    const isUserValid = inputValidate.usernameValidate(username);
    const isPasswordValid = inputValidate.passwordValidate(password);

    /*

        #TASK

        Validate the signup form
          a. Username should have minimum 6 characters with 2 uppercase, 2 lowercase & 2 numbers atleast (eg: F22Labs)
          b. On successful validation, show a dropdown alert with green background saying `User name exists`
          c. On unsuccessful validation, show a dropdown alert with red background saying `Invalid username`
          d. Email should have basic email validation
          e. Password should have minimum 8 characters
          f. Show corresponding error messages below every field

        NOTE: Making use of Formik & Yup libraries is preferred
      */

    if (
      isUserValid === 'empty_username' &&
      isPasswordValid === 'empty_password' &&
      isEmailValid === 'empty_email'
    ) {
      setInvalidCredential(false);
      setEmptyCredential(true);
      setEmptyUsername(false);
      setEmptyPassword(false);
      setInvalidUsername(false);
      setInvalidPassword(false);
    } else if (isUserValid === 'empty_username') {
      setInvalidCredential(false);
      setEmptyCredential(false);
      setEmptyUsername(true);
      setEmptyPassword(false);
      setInvalidUsername(false);
      setInvalidPassword(false);
    } else if (isPasswordValid === 'empty_password') {
      setInvalidCredential(false);
      setEmptyCredential(false);
      setEmptyUsername(false);
      setEmptyPassword(true);
      setInvalidUsername(false);
      setInvalidPassword(false);
    } else if (isEmailValid === 'empty_email') {
      setInvalidCredential(false);
      setEmptyCredential(false);
      setEmptyUsername(false);
      setEmptyPassword(false);
      setInvalidUsername(false);
      setInvalidPassword(false);
      setEmptyEmail(true);
    } else if (
      isUserValid === false &&
      isPasswordValid === false &&
      isEmailValid === false
    ) {
      setInvalidCredential(true);
      setEmptyCredential(false);
      setEmptyUsername(false);
      setEmptyPassword(false);
      setInvalidUsername(false);
      setInvalidPassword(false);
      setEmptyEmail(false);
      setInvalidEmail(false);
      setEmptyEmail(false);
    } else if (isUserValid === false) {
      setInvalidCredential(false);
      setEmptyCredential(false);
      setEmptyUsername(false);
      setEmptyPassword(false);
      setInvalidUsername(true);
      setInvalidPassword(false);
      setInvalidEmail(false);
      setEmptyEmail(false);
    } else if (isPasswordValid === false) {
      setInvalidCredential(false);
      setEmptyCredential(false);
      setEmptyUsername(false);
      setEmptyPassword(false);
      setInvalidUsername(false);
      setInvalidPassword(true);
      setInvalidEmail(false);
      setEmptyEmail(false);
    } else if (isEmailValid === false) {
      setInvalidCredential(false);
      setEmptyCredential(false);
      setEmptyUsername(false);
      setEmptyPassword(false);
      setInvalidUsername(false);
      setInvalidPassword(false);
      setInvalidEmail(true);
      setEmptyEmail(false);
    } else if (
      isUserValid === true &&
      isPasswordValid === true &&
      isEmailValid === true
    ) {
      setInvalidCredential(false);
      setEmptyCredential(false);
      setEmptyUsername(false);
      setEmptyPassword(false);
      setInvalidUsername(false);
      setInvalidPassword(false);
      setInvalidEmail(false);
      setEmptyEmail(false);
      ToastAndroid.show('submit', ToastAndroid.SHORT);
    } else {
      setInvalidCredential(true);
      setEmptyCredential(false);
      setEmptyUsername(false);
      setEmptyPassword(false);
      setInvalidUsername(false);
      setInvalidPassword(false);
      setInvalidEmail(false);
      setEmptyEmail(false);
    }
  };
  navigation.navigate(Tabs.name);

  return (
    <SignScaffold>
      <Image style={styles.logo} source={logo} />
      <View style={styles.form}>
        <View>
          <Text style={styles.headerTitle}>Sign up</Text>
          <Text style={styles.headerSubtitle}>
            Enter your credentials to continue
          </Text>
        </View>
        <KeyboardAvoidingView behavior={behavior}>
          <Input
            label="Username"
            onChangeText={(username) => setUsername(username)}
          />
          <View style={{marginTop: heightScreen * 0.011}} />
          <Input label="Email" onChangeText={(email) => setEmail(email)} />
          <View style={{marginTop: heightScreen * 0.011}} />
          <Input
            onChangeText={(password) => setPassword(password)}
            label="Password"
          />
        </KeyboardAvoidingView>

        <View style={styles.termsBox}>
          <Text style={styles.infoText}>
            By continuing you agree to our{' '}
            <Text style={[styles.infoText, styles.greenInfoText]}>
              Terms of Service
            </Text>{' '}
            and{' '}
            <Text style={[styles.infoText, styles.greenInfoText]}>
              Privacy Policy
            </Text>
            .
          </Text>
        </View>
        <Button
          onPress={goToHome}
          bgColour="#53B175"
          txtColour="#FFF"
          text="Sign up"
        />
      </View>
    </SignScaffold>
  );
};

const styles = EStyleSheet.create({
  logo: {
    alignSelf: 'center',
    marginTop: heightScreen * 0.032,
    marginBottom: heightScreen * 0.112,
  },
  form: {
    paddingHorizontal: widthScreen * 0.06,
  },
  headerTitle: {
    fontSize: '1.625rem',
    lineHeight: '1.625rem',
    height: '1.625rem',
    fontFamily: '$gilroyNormal600',
    marginBottom: heightScreen * 0.017,
    color: '$blackColour',
  },
  headerSubtitle: {
    fontSize: '1rem',
    lineHeight: '1rem',
    height: '1rem',
    fontFamily: '$gilroyMedium',
    marginBottom: heightScreen * 0.045,
    color: '$darkGreyColour',
  },
  termsBox: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginBottom: heightScreen * 0.033,
  },
  infoText: {
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: '$gilroyNormal600',
    fontWeight: '600',
    fontSize: '0.875rem',
    color: '$blackColour',
    letterSpacing: '0.05rem',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: heightScreen * 0.028,
  },
  greenInfoText: {
    color: '$greenColour',
    marginLeft: 5.0,
  },
});

export default {component: SignUp, name: 'SignUp'};
