import React, {useState, useEffect} from 'react';
import {View, ScrollView, Dimensions, Image} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import LogoColour from '../../../../assets/images/logo-colour.png';
import Banner from '../../../../assets/images/home_screen/banner.png';
import SearchBar from '../../../components/SearchBar';
import SectionTitle from './SectionTitle';
import FoodCard from '../../../components/FoodCard';
import Api from '../../../services/Api';
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');

const Home = () => {
  const [exclusiveProducts, setExclusiveProducts] = useState([]);
  const [bestsellingProducts, setBestSellingProducts] = useState([]);

  useEffect(() => {
    console.log('Home js');
    getExclusive();
    getBestSelling();
  }, []);

  const getExclusive = () => {
    Api.getExclusiveProducts()
      .then((response) => {
        const products = response !== undefined ? response : {};
        //console.log('exclusive products', JSON.stringify(products.data));

        setExclusiveProducts(products.data);
      })
      .catch((error) => {
        console.log('exclusive prodcuct error', error);
      });
  };
  const getBestSelling = () => {
    Api.getBestSellingProducts()
      .then((response) => {
        const products = response !== undefined ? response : {};
        console.log('best selling products', JSON.stringify(products.data));

        setBestSellingProducts(products.data);
      })
      .catch((error) => {
        console.log('best selling prodcuct error', error);
      });
  };

  return (
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <Image source={LogoColour} style={styles.logo} />
      </View>
      <View style={[styles.localBox, styles.searchBox]}>
        <SearchBar />
      </View>
      <View style={styles.localBox}>
        <Image style={styles.banner} source={Banner} />
      </View>

      {/*

        #TASK

        Integrate API
          a. Endpoints available at helpers/endPoints.ts
          b. Replace the static piece of code with data from API

      */}

      {exclusiveProducts.map((data) => (
        <>
          <View style={styles.localBox} key={data.sectionId}>
            <SectionTitle title={data.sectionTitle} />
          </View>
          <ScrollView style={styles.horizontalScroll} horizontal={true}>
            {data.sectionItems.map((item, j) => (
              <FoodCard
                key={item.id}
                title={item.title}
                desc={item.desc}
                price={item.price}
                imageUrl={item.imageUrl}
              />
            ))}
          </ScrollView>
        </>
      ))}
      {bestsellingProducts.map((data, i) => (
        <>
          <View style={styles.localBox} key={data.sectionId}>
            <SectionTitle title={data.sectionTitle} />
          </View>
          <ScrollView style={styles.horizontalScroll} horizontal={true} >
            {data.sectionItems.map((item) => (
              <FoodCard
                key={item.id}
                title={item.title}
                desc={item.desc}
                price={item.price}
                imageUrl={item.imageUrl}
              />
            ))}
          </ScrollView>
        </>
      ))}
      <View style={styles.scrollFooter} />
    </ScrollView>
  );
};

const styles = EStyleSheet.create({
  localBox: {
    paddingHorizontal: 25.0,
  },
  container: {
    width: widthScreen,
    minHeight: heightScreen,
    paddingTop: 35.0,
    backgroundColor: '$whiteColour',
  },
  header: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  logo: {
    width: 36.48,
    height: 40.0,
  },
  searchBox: {
    marginTop: 20.0,
  },
  banner: {
    marginTop: 20.0,
    width: widthScreen * 0.87,
    resizeMode: 'contain',
  },
  horizontalScroll: {
    paddingLeft: 20.0,
    paddingBottom: 10.0,
  },
  scrollFooter: {
    marginBottom: heightScreen * 0.2,
    backgroundColor: 'red',
  },
});

export default {component: Home, name: 'Home'};
