import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const {width: widthScreen, height: heightScreen} = Dimensions.get('window');

const CartTab = () => {
  return (
    <View style={styles.container}>
      {/*

        #TASK

        Create the UI for cart screen
          a. Fetch the list of cart items from endPoint available in helpers/Endpoints.ts
          b. Refer prints folder cart screen UI & replicate it as such.
          c. Changing the quantity & removing the item functionality should be implemented.
          c. The screen should be scrollable if there are a lot of items on cart.
          d. Clicking on `Go to checkout` should take user to OrderAccepted screen. Refer /screens/status/OrderAccepted.tsx & prints/screen_order_accepted.png
          e. Handle `no items in cart` UI too

      */}
      <Text>My Cart</Text>
    </View>
  );
};

const styles = EStyleSheet.create({
  container: {
    width: widthScreen,
    minHeight: heightScreen,
    paddingTop: 35.0,
    backgroundColor: '$whiteColour',
    display: 'flex',
    alignItems: 'center',
  },
});

export default {component: CartTab, name: 'Cart'};
