import React, {useState} from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Alert,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ProfileImage from '../../../assets/images/profile.png';
import {AccountIcons} from '../../helpers/Icons';
import AccountListItem from '../../components/AccountListItem';
import Button from '../../components/Button';
import Onboarding from '../Onboarding';
import {launchImageLibrary} from 'react-native-image-picker';
import AppPermission from '../../services/AppPermission';
const {width: screenWidth, height: screenHeight} = Dimensions.get('screen');

interface AccountProps {
  navigation: any;
}

const AccountTab = ({navigation}: AccountProps) => {
  const [accountPath, setAccountPath] = useState({});
  const itemList = [
    {
      label: 'My Details',
      icon: (
        <AccountIcons.PersonalCardIcon style={styles.icon} color={'#181725'} />
      ),
    },
    {
      label: 'Orders',
      icon: <AccountIcons.OrdersIcon style={styles.icon} color={'#181725'} />,
    },
    {
      label: 'Delivery Address',
      icon: <AccountIcons.PinIcon style={styles.icon} color={'#181725'} />,
    },
    {
      label: 'Payment Methods',
      icon: <AccountIcons.PaymentIcon color={'#181725'} />,
    },
    {
      label: 'Notifications',
      icon: <AccountIcons.BellIcon color={'#181725'} />,
    },
    {
      label: 'About',
      icon: <AccountIcons.AboutIcon color={'#181725'} />,
    },
  ];

  const goToOnboarding = () => {
    navigation.navigate(Onboarding.name);
  };

  const chooseFile = (type) => {
    let options = {
      mediaType: type,
      maxWidth: 300,
      maxHeight: 550,
      quality: 1,
    };
    launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        alert('User cancelled camera picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        alert(response.errorMessage);
        return;
      }
      console.log('base64 -> ', response.base64);
      console.log('uri -> ', response.uri);
      console.log('width -> ', response.width);
      console.log('height -> ', response.height);
      console.log('fileSize -> ', response.fileSize);
      console.log('type -> ', response.type);
      console.log('fileName -> ', response.fileName);

      console.log('choose', response.assets[0].uri);
      setAccountPath(response.assets[0].uri);
    });
  };

  const selectPicture = (type) => {
    AppPermission.plaformPermissions()
      .then((res) => {
        const requestResponse = res !== undefined ? res : {};

        if (requestResponse === true) {
          chooseFile(type);
        } else {
          return;
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };

  return (
    <>
      <ScrollView style={styles.scrollContainer}>
        <View style={styles.headerContainer}>
          {/*

            #TASK

            Integrate image picker
              a. Ask user permission to access camera/media usage
              b. If permission denied, show alert if they want to close it & take them to settings to enable it***
              c. Picker to restrict only images
              d. Show preview of the selected image

          */}

          <TouchableOpacity onPress={() => selectPicture('photo')}>
            {Object.keys(accountPath).length !== 0 ? (
              <Image
                style={styles.headerImage}
                source={{uri: `${accountPath}`}}
              />
            ) : (
              <Image style={styles.headerImage} source={ProfileImage} />
            )}
          </TouchableOpacity>

          <View style={styles.textBox}>
            <View style={styles.headerTitleBox}>
              <Text style={styles.headerTitle}>F22Labs</Text>
            </View>
            <Text style={styles.headerSubtitle}>test@f22labs.com</Text>
          </View>
        </View>
        <View style={styles.list}>
          {itemList.map((item, index) => {
            return (
              <AccountListItem
                key={index}
                label={item.label}
                children={item.icon}
              />
            );
          })}
        </View>
        <View style={styles.buttonBox}>
          <Button
            onPress={goToOnboarding}
            text="Log Out"
            bgColour="#F2F3F2"
            txtColour="#53B175"
          />
        </View>
      </ScrollView>
    </>
  );
};

const styles = EStyleSheet.create({
  scrollContainer: {
    backgroundColor: '$whiteColour',
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: screenWidth * 0.06,
    paddingTop: screenHeight * 0.1,
    paddingBottom: screenHeight * 0.033,
    borderBottomWidth: 1.0,
    borderBottomColor: '$lightGreyColour',
  },
  headerImage: {
    width: screenHeight * 0.171,
    height: screenHeight * 0.171,
    borderRadius: screenHeight * 0.171,
  },
  textBox: {
    marginLeft: screenWidth * 0.05,
  },
  headerTitleBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerTitle: {
    fontFamily: '$gilroyNormal600',
    fontWeight: '600',
    fontSize: '1.125rem',
    color: '$blackColour',
    marginRight: screenWidth * 0.024,
  },
  headerSubtitle: {
    fontFamily: '$gilroyNormal',
    fontSize: '0.875rem',
    color: '$darkGreyColour',
  },
  headerIcon: {
    color: '$greenColour',
  },
  list: {
    paddingBottom: screenHeight * 0.058,
  },
  buttonBox: {
    paddingBottom: screenHeight * 0.027,
    paddingHorizontal: screenWidth * 0.06,
  },
  icon: {
    color: '#181725',
  },
});

export default {component: AccountTab, name: 'Account'};
