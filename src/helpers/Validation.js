export default class inputValidation {
  constructor() {}

  emailValidate = (text) => {
    let reg = /^\w+([.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (text === '') {
      return 'empty_email';
    } else if (reg.test(text) === false) {
      return false;
    } else {
      return true;
    }
  };

  passwordValidate = (pass) => {
    if (pass === '') {
      return 'empty_password';
    } else if (pass.length < 8) {
      return false;
    } else {
      return true;
    }
  };

  mobileValidate = (num) => {
    let mob = /^[1-9]{1}[0-9]{9}$/;

    if (num === '') {
      return ' empty_mobile';
    } else if (mob.test(num) === false) {
      return false;
    } else {
      return true;
    }
  };

  usernameValidate = (username) => {
    if (username === '') {
      return 'empty_username';
    } else if (username.length < 6) {
      return false;
    } else {
      return true;
    }
  };

  checkUserName = (str) => {
    if (str.length < 6) {
      return 'too_short';
    } else if (str.length > 50) {
      return 'too_long';
    } else if (str.search(/\d/) == -1) {
      return 'no_num';
    } else if (str.search(/[a-zA-Z]/) == -1) {
      return 'no_letter';
    } else if (str.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1) {
      return 'bad_char';
    }
    return 'ok';
  };
}
