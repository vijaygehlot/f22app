import * as axios from 'axios';

class Api {
  constructor() {
    this.api_url = 'https://jsonblob.com/api/jsonBlob/';
  }

  init = () => {
    let headers = {
      Accept: 'application/json',
      //Authorization: `Bearer ${token}`,
    };

    this.client = axios.create({
      baseURL: this.api_url,
      headers: headers,
    });

    return this.client;
  };

  getExclusiveProducts = () => {
    return new Promise((resolve, reject) => {
      this.init()
        .get('/8020f261-cf9d-11eb-a671-9ba03453d647')
        .then((response) => {
          const responseData = response !== undefined ? response : {};

          if (responseData !== undefined) {
            resolve(responseData);
          } else {
            reject({});
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
  getBestSellingProducts = () => {
    return new Promise((resolve, reject) => {
      this.init()
        .get('/24c64a47-cf9a-11eb-a671-a391734c1497')
        .then((response) => {
          const responseData = response !== undefined ? response : {};

          if (responseData !== undefined) {
            resolve(responseData);
          } else {
            reject({});
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
}

export default new Api();
