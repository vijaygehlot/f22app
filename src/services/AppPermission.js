import {Platform} from 'react-native';
import {PERMISSIONS, RESULTS, request} from 'react-native-permissions';
import PermissionANDROID from './PermissionAndroid';
import PermissionIOS from './PermissionIos';
class Permission {
  constructor() {}

  plaformPermissions = () => {
    return new Promise((resolve, reject) => {
      Platform.OS === 'ios'
        ? PermissionIOS.requestMultiplePermissions()
            .then((response) => {
              const allReqPermissionResponse =
                response !== undefined ? response : {};

              if (allReqPermissionResponse === true) {
                resolve(true);
              } else {
                reject({});
              }
            })
            .catch((error) => {
              reject(error);
            })
        : PermissionANDROID.reuestMultiplePermissions()
            .then((response) => {
              const allCheckPermissionResponse =
                response !== undefined ? response : {};
              console.log(
                'allRequestPermissionResponse',
                allCheckPermissionResponse,
                response,
              );
              if (allCheckPermissionResponse === true) {
                resolve(true);
              } else {
                reject({});
              }
            })
            .catch((error) => {
              reject(error);
            });
    });
  };
}

export default new Permission();
