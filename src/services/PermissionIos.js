import {PERMISSIONS, RESULTS, requestMultiple} from 'react-native-permissions';

class PermissionIOS {
  constructor() {}

  requestMultiplePermissions = () => {
    return new Promise((resolve, reject) => {
      requestMultiple([
        PERMISSIONS.IOS.PHOTO_LIBRARY,
        PERMISSIONS.IOS.PHOTO_LIBRARY_ADD_ONLY,
      ])
        .then((statusIOS) => {
          if (
            statusIOS[PERMISSIONS.IOS.PHOTO_LIBRARY] === RESULTS.GRANTED &&
            statusIOS[PERMISSIONS.IOS.PHOTO_LIBRARY_ADD_ONLY] ===
              RESULTS.GRANTED
          ) {
            resolve(true);
          } else {
            reject({});
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
}

export default new PermissionIOS();
