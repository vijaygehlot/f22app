import {PERMISSIONS, RESULTS, requestMultiple} from 'react-native-permissions';
import {Platform} from 'react-native';

class PermissionANDROID {
  constructor() {}

  reuestMultiplePermissions = () => {
    return new Promise((resolve, reject) => {
      requestMultiple([
        PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
        PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
      ])
        .then((statusAndroid) => {
          if (
            statusAndroid[PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE] ===
              RESULTS.GRANTED &&
            statusAndroid[PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE] ===
              RESULTS.GRANTED
          ) {
            resolve(true);
          } else {
            reject({});
          }
        })
        .catch((error) => {
          reject(error);
        });
    });
  };
}

export default new PermissionANDROID();
